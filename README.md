Requirements
------------

* PHP 8.1+
* Enable Soap PHP Extension

Installation
------------

configure .env file and

```
php bin/console doctrine:database:create
php bin/console doctrine:schema:create
composer install
```

Run
------------

```
php8 -S localhost:8080 -t public\
```

Api Doc Access
------------

http://localhost:8080/api