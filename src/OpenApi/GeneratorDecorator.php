<?php

declare(strict_types=1);

namespace App\OpenApi;

use ApiPlatform\OpenApi\Factory\OpenApiFactoryInterface;
use Symfony\Component\DependencyInjection\Attribute\Autowire;
use ApiPlatform\OpenApi\OpenApi;
use ApiPlatform\OpenApi\Model;

use Symfony\Component\Yaml\Yaml;
use Symfony\Component\Finder\Finder;

final class GeneratorDecorator implements OpenApiFactoryInterface
{
    public function __construct(
        private OpenApiFactoryInterface $decorated,
        #[Autowire('%kernel.project_dir%')]
        private string $projectDir
    ){}

    public function __invoke(array $context = []): OpenApi
    {

        $finder = new Finder();
        $finder->files()->in($this->projectDir.'/src/OpenApi/operations');

        $openApi = ($this->decorated)($context);

        foreach ($finder as $file) {

            $file = $file->getRealPath();
            $yaml = Yaml::parseFile($file);

            foreach ($yaml as $operationID => $operation)
            {
                $responses = [];
                foreach ($operation['responses'] as $code => $responsesYaml) {
                    $responses[$code] = [
                        'content' => [
                            'application/json' => [
                                'example' => $responsesYaml
                            ]
                        ]
                    ];
                }

                $parameters = [];
                if(array_key_exists('parameters', $operation))
                    foreach ($operation['parameters'] as $parameterName => $parametersYaml)
                        $parameters[] = new Model\Parameter($parameterName, 'query', $parametersYaml['description'], $parametersYaml['required']);

                if(array_key_exists('body', $operation)){
                    $requestBody = new Model\RequestBody(
                        content:  new \ArrayObject([
                            'application/json' => [
                                'schema'  => [
                                    'type'       => 'object',
                                    'properties' => $operation['schema']
                                ],
                                'example' => $operation['body'],
                            ],
                        ]),
                    );
                }

                $pathItem = $openApi->getPaths()->getPath($operation['path']) ?: new Model\PathItem();

                $newOperation = new Model\Operation(
                    operationId: $operationID,
                    tags: $operation['tags'],
                    responses: $responses,
                    description: $operation['description'],
                );

                $pathItem = match(strtolower($operation['method'])){
                    'get' => $pathItem->withGet($newOperation->withParameters($parameters)),
                    'post' => $pathItem->withPost($newOperation->withRequestBody($requestBody)),
                    'delete' => $pathItem->withDelete($newOperation->withRequestBody($requestBody)),
                };

                $openApi->getPaths()->addPath($operation['path'], $pathItem);
            }
        }

        return $openApi;
    }
}