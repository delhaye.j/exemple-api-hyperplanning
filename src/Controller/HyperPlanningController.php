<?php

namespace App\Controller;


use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Attribute\AsController;
use Symfony\Component\Routing\Annotation\Route;

#[AsController]
class HyperPlanningController extends AbstractController
{
    private \SoapClient $client;
    public function __construct() {
        /* config soap parameters */
        ini_set('soap.wsdl_cache_enabled',0);
        ini_set('soap.wsdl_cache_ttl',0);
        /* Connect the soap client */
        $this->client = new \SoapClient($_ENV['HP_SOAP_WSDL'], array('login'=> $_ENV['HP_SOAP_USER'],'password'=> $_ENV['HP_SOAP_PASSWORD']));
        $this->client->__setLocation($_ENV['HP_SOAP_WSDL']);
    }
    #[Route(
        path: '/hp/reservation',
        methods: ['POST']
    )]
    public function addOrUpdateReservation(Request $request) : JsonResponse
    {
        $parameters = json_decode($request->getContent(), true);
        $salle = $this->client->AccederSalleParNomEtCode($parameters['room'] ,$parameters['room']);
        $listReservations = $this->getListReservations($salle);

        $IdsHpCours = $this->getHpCoursID($listReservations,$parameters['id']);

        //if Cours exist in HP delete before add new
        if($IdsHpCours)
            foreach ($IdsHpCours as $id)
                $this->client->SupprimerCours($id);

        $startDate = new \DateTime($parameters['startDate']);
        $endDate = new \DateTime($parameters['endDate']);
        $duration = $startDate->diff($endDate);

        // if reservation of one on more full day
        if($duration->d >= 1){
            for($i = 0; $i < $duration->d; $i++){
                $startDate->modify('+'.$i.' day');
                $startDate = \DateTime::createFromFormat('Y-m-d H:i:s', $startDate->format("Y-m-d") . " 06:00:00");
                $endDate = \DateTime::createFromFormat('Y-m-d H:i:s', $startDate->format("Y-m-d") . " 19:00:00");
                $startDate->setTimezone(new \DateTimeZone( 'Europe/Brussels'));
                $endDate->setTimezone(new \DateTimeZone( 'Europe/Brussels'));
                $this->createReservation($parameters['id'],$salle,$startDate, $endDate);
            }
        } else {
            // simple case : not all day
            /* clamp hour value */
            if(intval($startDate->format("H")) < 6)
                $startDate = \DateTime::createFromFormat('Y-m-d H:i:s', $startDate->format("Y-m-d") . " 06:00:00");
            if(intval($endDate->format("H")) > 19)
                $endDate = \DateTime::createFromFormat('Y-m-d H:i:s', $endDate->format("Y-m-d") . " 19:00:00");
            // formating date to correct time zone (same as hyperplanning)
            $startDate->setTimezone(new \DateTimeZone( 'Europe/Brussels'));
            $endDate->setTimezone(new \DateTimeZone( 'Europe/Brussels'));
            $this->createReservation($parameters['id'],$salle,$startDate, $endDate);
        }

        return new JsonResponse(['status' => "success"]);
    }
    #[Route(
        path: '/hp/reservation',
        methods: ['DELETE']
    )]
    public function deleteReservation(Request $request) : JsonResponse
    {
        $parameters = json_decode($request->getContent(), true);
        $this->deleteReservation2($parameters['id'] ,$parameters['room']);
        return new JsonResponse(['status' => "success"]);
    }
    public function deleteReservation2(string $id, string $room)
    {
        $salle = $this->client->AccederSalleParNomEtCode($room ,$room);
        $listReservations = $this->getListReservations($salle);
        $IdsHpCours = $this->getHpCoursID($listReservations,$id);

        //if Cours exist in HP delete before add new
        if($IdsHpCours)
            foreach ($IdsHpCours as $id)
                $this->client->SupprimerCours($id);
    }
    public function createReservation($idReservation, $salle, $startDate,$endDate) : bool
    {
        $duration = $startDate->diff($endDate);
        $durationHp = $this->client->HeureMinuteEnHpSvcWDuree($duration->h,$duration->i);
        $week = $this->client->DateEnSemaine($startDate->format('Y-m-d'));

        $mondayWeek = new \DateTime($this->client->SemaineEnDate($week),new \DateTimeZone('Europe/Brussels'));

        $durationHpFromMonday = $mondayWeek->diff($startDate);

        $durationHpFromMonday = $this->client->HeureMinuteEnHpSvcWDuree(($durationHpFromMonday->d*24)+$durationHpFromMonday->h,$durationHpFromMonday->i);

        $newReservation = $this->client->CreerCoursFixe(
            2631, // AMatiere : THpSvcWCleMatiere
            $durationHp, //ADuree : THpSvcWDuree
            [], // ATableauEnseignant : THpSvcWTableauClesEnseignants
            [], // ATableauGroupe : THpSvcWTableauClesRegroupements
            [], // ATableauPromotion : THpSvcWTableauClesPromotions
            [], // ATableauTDOption : THpSvcWTableauClesTDOptions
            [$salle],// ATableauSalle : THpSvcWTableauClesSalles
            [$week],// ADomainePotentiel : THpSvcWTableauSemaines
        );
        $this->client->ModifierMemoCours($newReservation,$idReservation,[$week]);

        $this->client->ModifierPlaceCours($newReservation,$durationHpFromMonday,[$week],true);

        return true;
    }
    private function getListReservations($salle) : array
    {
        $cours = $this->client->CoursSalle($salle);

        $memos = $this->client->MemosTableauDeCours($cours);

        $memosCours = array();

        if(property_exists($cours,"THpSvcWCleCours"))
            if(is_array($cours->THpSvcWCleCours))
                foreach ($cours->THpSvcWCleCours as $key => $value)
                    $memosCours[$memos->string[$key]][] = $value;
            else
                $memosCours[$memos->string] = $cours->THpSvcWCleCours;
        return $memosCours;
    }
    private function getHpCoursID($listReservations, $IdOutlook) : array
    {
        foreach ($listReservations as $key => $value){
            if($key == $IdOutlook)
                return $value;
        }
        return [];
    }
    #[Route(
        path: '/hp/student/create',
        methods: ['POST']
    )]
    public function createStudent(Request $request) : JsonResponse
    {
        $parameters = json_decode($request->getContent(), true);

        try
        {
            $newStudent = $this->client->CreerEtudiantAvecIdentifiant(
                $parameters['name'],// ANom : string
                $parameters['firstname'],// APrenom : string
                "2000-01-01", // ADateDeNaissance : date
                $parameters['username'],// AIdentifiant : string
            );
        }
        catch (\SoapFault $e)
        {
            switch ($e->getCode()) {
                case 0:
                    return new JsonResponse(['status' => "error", "message" => 'username already exist']);
                    break;
                default:
                    return new JsonResponse(['status' => "error", "message" => 'unknown error']);
            }
        }

        return new JsonResponse(['status' => "success"]);
    }
}